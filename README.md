# PluXML to Gemtext

A python script to convert PluXML post and static page to gemtext file, as well as the index page, ready to be serve by any Gemini server.

For any more information about the Gemini Protocol, check the [official website](https://gemini.circumlunar.space/)

## Installation

```
git clone https://framagit.org/blchrd/pluxml-to-gemtext
cd pluxml-to-gemtext
pip install -r requirements.txt
```

## Usage

Change the value in the script if needed

Update `index.template` with your index template.

```
python main.py [pluxml-data-folder]
```

*All converted files will be outputed in `gmi_files` by default, you can change this dir in the script.*

**Caution, importing comments, tags and categories are not supported**

