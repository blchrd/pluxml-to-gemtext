import sys
import os
import untangle
from pathlib import Path
from markdownify import markdownify
from md2gemini import md2gemini
from bs4 import BeautifulSoup

# ========= CHANGE THIS =========== #
root_gmi = "gmi_files"
author = "ndrrk"
host = "gemini://localhost/"
with open('index.template') as template:
    index_template = template.read()
# ================================= #

data_dir = sys.argv[1]
articles_dir = f"{data_dir}/articles"
statics_dir = f"{data_dir}/statiques"
configuration_dir = f"{data_dir}/configuration"
blog_posts = []
pages = []


def create_index_page(blog_post_list, page_list):
    print(f"Create index links list in {root_gmi}/index.gmi")
    blog_post_links = ""
    page_links = ""
    for blog_post in blog_post_list:
        blog_post_links += f"=> {blog_post}\r\n"
    for page in page_list:
        page_links += f"=> {page}\r\n"
    gemini_index = index_template.format(blog_post_list=blog_post_links, page_list=page_links)

    with open(f"{root_gmi}/index.gmi", 'w', encoding='utf-8') as wf:
        wf.write(gemini_index)


def html_to_markdown(html):
    soup = BeautifulSoup(html, features="html.parser")
    iframes = soup.find_all('iframe')
    for element in iframes:
        element.extract()

    html = str(soup)
    html = html.replace("<p></p>", "")

    return markdownify(html)


def convert_xml_article_to_gmi(xml_file_path):
    obj = untangle.parse(xml_file_path)
    title = obj.document.title.cdata
    html_content = obj.document.content.cdata
    date_creation = obj.document.date_creation.cdata

    formatted_creation_date = f"{date_creation[0:4]}-{date_creation[4:6]}-{date_creation[6:8]}"

    author_and_date = f"Author: {author} - Date: {formatted_creation_date}\r\n"

    markdown_content = html_to_markdown(html_content)
    markdown_content = f"# {title}\r\n{author_and_date}\r\n{markdown_content}"

    gemini_content = md2gemini(markdown_content, links="at-end", plain=True)

    gemini_file = formatted_creation_date + "-" + file_title_array[len(file_title_array) - 2] + ".gmi"

    gemini_file_path = f'{root_gmi}/{gemini_file}'
    with open(gemini_file_path, 'w', encoding='utf-8') as wf:
        print(f'Convert {xml_file_path} to {gemini_file_path}')
        wf.write(gemini_content)
        blog_posts.append(f'{host}{gemini_file}  {formatted_creation_date}: {title}')


def convert_php_static_page_to_gmi(static_obj):
    static_page_file = f"{static_obj['number']}.{static_obj['url']}.php"
    title = static_obj.name.cdata
    gemini_file = title.lower().replace(' ', '-')
    with open(f'{statics_dir}/{static_page_file}', 'r', encoding='utf-8') as f:
        html_content = f.read()
        markdown_content = html_to_markdown(html_content)
        markdown_content = f"# {title}\r\n{markdown_content}"

        gemini_content = md2gemini(markdown_content, links="at-end", plain=True)
        gemini_file = gemini_file + ".gmi"
        gemini_file_path = f'{root_gmi}/{gemini_file}'
        with open(gemini_file_path, 'w', encoding='utf-8') as wf:
            print(f'Convert {statics_dir}/{static_page_file} to {gemini_file_path}')
            wf.write(gemini_content)
            pages.append(f'{host}{gemini_file}  {title}')


Path(root_gmi).mkdir(parents=True, exist_ok=True)
static_xml_conf = untangle.parse(f"{configuration_dir}/statiques.xml")
for static_page in static_xml_conf.document.statique:
    convert_php_static_page_to_gmi(static_page)

for root, dirs, files in os.walk(articles_dir):
    for file in files:
        if not file.endswith('.xml'):
            continue

        file_title_array = file.split(".")
        if file_title_array[1].startswith('draft,'):
            continue

        file_path = f'{root}/{file}'
        convert_xml_article_to_gmi(file_path)

blog_posts.sort(reverse=True)
create_index_page(blog_posts, pages)
